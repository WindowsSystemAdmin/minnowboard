#
# Copyright 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Rule to make a distribution zipfile with all that is needed to flash the MinnowBoard

ifeq ($(TARGET_DEVICE),minnowboard)

minnowboardname := $(TARGET_PRODUCT)
ifeq ($(TARGET_BUILD_TYPE),debug)
  minnowboardname := $(minnowboardname)_debug
endif
minnowboardname := $(minnowboardname)-flashfiles-$(FILE_NAME_TAG)

MINNOWBOARD_ZIP    :=  $(TARGET_OUT_INTERMEDIATES)/$(minnowboardname).zip
MINNOWBOARD_VENDOR := vendor/bsp/intel/minnowboard
MINNOWBOARD_BINARIES := $(MINNOWBOARD_VENDOR)/boot_binaries
MINNOWBOARD_DEVICE := device/intel/minnowboard
MINNOWBOARD_TOOLS  := $(MINNOWBOARD_DEVICE)/flash_tools

MINNOWBOARD_FLASHFILES += $(MINNOWBOARD_BINARIES)/bootloader
MINNOWBOARD_FLASHFILES += $(MINNOWBOARD_BINARIES)/oemvars.txt
MINNOWBOARD_FLASHFILES += $(MINNOWBOARD_TOOLS)/brillo-flashall.sh
MINNOWBOARD_FLASHFILES += $(MINNOWBOARD_TOOLS)/README
MINNOWBOARD_FLASHFILES += $(PRODUCT_OUT)/partition-table.img
MINNOWBOARD_FLASHFILES += $(PRODUCT_OUT)/boot.img
MINNOWBOARD_FLASHFILES += $(PRODUCT_OUT)/system.img
MINNOWBOARD_FLASHFILES += $(PRODUCT_OUT)/userdata.img

# Include the usb-stick fastboot image
MINNOWBOARD_FLASHFILES += $(MINNOWBOARD_BINARIES)/fastboot-usb.img

# Include fastboot and adb - so that the latest is available
MINNOWBOARD_FLASHFILES += $(HOST_OUT)/bin/fastboot
MINNOWBOARD_FLASHFILES += $(HOST_OUT)/bin/adb

$(MINNOWBOARD_ZIP): $(MINNOWBOARD_FLASHFILES)
	$(hide) echo "Package flashfiles: $@"
	$(hide) rm -rf $@
	$(hide) mkdir -p $(dir $@)
	$(hide) zip -j $@ $(MINNOWBOARD_FLASHFILES)

$(call dist-for-goals, dist_files, $(MINNOWBOARD_ZIP))

endif
